package co.uk.goodpoint.util.stream;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static co.uk.goodpoint.util.stream.CustomCollectorsTest.Payment.payment;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class CustomCollectorsTest {

    @Test
    public void shouldSumUsingCustomSumFunctions() {
        List<String> votes = asList(
                "candidate1",
                "candidate2",
                "candidate1",
                "candidate1",
                "candidate3",
                "candidate2"
        );

        // When
        Map<String, Integer> actualMap = votes.stream().collect(Collectors.groupingBy(
                vote -> vote,
                HashMap::new,
                CustomCollectors.summing(vote -> 1, () -> 0, (a, b) -> a + b)
        ));

        // Then
        Map<String, Integer> expectedMap = new HashMap<>();
        expectedMap.put("candidate1", 3);
        expectedMap.put("candidate2", 2);
        expectedMap.put("candidate3", 1);

        assertThat(actualMap, equalTo(expectedMap));
    }

    @Test
    public void shouldBeAbleToAggregateBigDecimals() throws Throwable {
        List<Payment> payments = asList(
                payment("user1", "48.3"),
                payment("user2", "12.8"),
                payment("user3", "153.2"),
                payment("user1", "0.15"),
                payment("user2", "15.5"),
                payment("user2", "19.27")
        );

        // When
        Map<String, BigDecimal> actualMap = payments.stream().collect(Collectors.groupingBy(
                Payment::getPayee,
                HashMap::new,
                CustomCollectors.summingBigDecimals(Payment::getAmount)
        ));

        // Then
        Map<String, BigDecimal> expectedMap = new HashMap<>();
        expectedMap.put("user1", new BigDecimal("48.45"));
        expectedMap.put("user2", new BigDecimal("47.57"));
        expectedMap.put("user3", new BigDecimal("153.2"));

        assertThat(actualMap, equalTo(expectedMap));
    }


    public static class Payment {

        private final String payee;
        private final BigDecimal amount;

        public Payment(String payee, BigDecimal amount) {
            this.payee = payee;
            this.amount = amount;
        }

        public static Payment payment(String payee, String amount) {
            return new Payment(payee, new BigDecimal(amount));
        }

        public String getPayee() {
            return payee;
        }

        public BigDecimal getAmount() {
            return amount;
        }
    }
}
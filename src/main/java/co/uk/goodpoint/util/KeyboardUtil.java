package co.uk.goodpoint.util;

import java.awt.*;

import static java.awt.event.KeyEvent.*;

public class KeyboardUtil {

    public static void pressKeysTogether(int... keys) {
        Robot robot = createRobot();

        for (int key : keys) {
            robot.keyPress(key);
        }

        robot.delay(75);

        for (int i = keys.length - 1; i >= 0; i--) {
            robot.keyRelease(keys[i]);
        }

        robot.delay(15);
    }

    public static void pressKeys(int... keys) {
        Robot robot = createRobot();

        for (int key : keys) {
            robot.keyPress(key);
            robot.delay(75);
            robot.keyRelease(key);
            robot.delay(15);
        }
    }

    public static void pressAltTab() {
        pressKeysTogether(VK_ALT, VK_TAB);
    }

    public static void pressAltPrintScreen() {
        pressKeysTogether(VK_ALT, VK_PRINTSCREEN);
    }

    private static Robot createRobot() {
        try {
            return new Robot();
        } catch (AWTException e) {
            throw new RuntimeException(e);
        }
    }
}

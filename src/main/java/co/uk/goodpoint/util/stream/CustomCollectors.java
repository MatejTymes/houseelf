package co.uk.goodpoint.util.stream;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;
import java.util.function.*;
import java.util.stream.Collector;

public class CustomCollectors {

    public static <F, T> Collector<F, ?, T> summing(Function<F, T> objectToValue, final Supplier<T> zeroValueProvider, BiFunction<T, T, T> sumFunction) {
        return new Collector<F, Holder<T>, T>() {
            @Override
            public Supplier<Holder<T>> supplier() {
                return () -> new Holder<T>(zeroValueProvider.get());
            }

            @Override
            public BiConsumer<Holder<T>, F> accumulator() {
                return (holder, object) -> holder.value = sumFunction.apply(holder.value, objectToValue.apply(object));
            }

            @Override
            public BinaryOperator<Holder<T>> combiner() {
                return (holder1, holder2) -> {
                    holder1.value = sumFunction.apply(holder1.value, holder2.value);
                    return holder1;
                };
            }

            @Override
            public Function<Holder<T>, T> finisher() {
                return holder -> holder.value;
            }

            @Override
            public Set<Characteristics> characteristics() {
                return Collections.emptySet();
            }
        };
    }

    public static <F> Collector<F, ?, BigDecimal> summingBigDecimals(Function<F, BigDecimal> objectToBigDecimal) {
        return summing(objectToBigDecimal, () -> BigDecimal.ZERO, BigDecimal::add);
    }

    private static class Holder<T> {

        public T value;

        public Holder(T value) {
            this.value = value;
        }
    }
}

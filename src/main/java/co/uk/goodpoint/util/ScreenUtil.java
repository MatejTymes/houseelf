package co.uk.goodpoint.util;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ScreenUtil {

    public static Dimension getScreenSize() {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }

    public static Rectangle getWholeScreenRectangle() {
        return new Rectangle(getScreenSize());
    }

    private static BufferedImage captureScreen(Rectangle screenRectangle) {
        BufferedImage capturedScreen = null;
        try {
            capturedScreen = new Robot().createScreenCapture(screenRectangle);
        } catch (AWTException e) {
            throw new RuntimeException(e);
        }
        return capturedScreen;
    }

    public static BufferedImage captureScreen() {
        return captureScreen(getWholeScreenRectangle());
    }
}

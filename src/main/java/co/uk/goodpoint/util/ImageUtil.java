package co.uk.goodpoint.util;

import sun.awt.image.ToolkitImage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static java.awt.RenderingHints.*;
import static java.awt.image.BufferedImage.TYPE_INT_ARGB;
import static java.awt.image.BufferedImage.TYPE_INT_RGB;

public class ImageUtil {

    public static BufferedImage loadImage(File file) {
        try {
            return ImageIO.read(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static BufferedImage getImageFromClipboard() {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        try {
            return (BufferedImage) clipboard.getData(DataFlavor.imageFlavor);
        } catch (UnsupportedFlavorException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static BufferedImage resizeImage(BufferedImage originalImage, int newWidth, int newHeight, boolean higherQuality) {
        // maybe use Thumbnailator instead: https://code.google.com/p/thumbnailator/
//        try {
//            return Thumbnails.of(originalImage).size(newWidth, newHeight).asBufferedImage();
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }

        int type = (originalImage.getTransparency() == Transparency.OPAQUE) ? TYPE_INT_RGB : TYPE_INT_ARGB;

        // this is extremely slow algorithm
//        if (true) {
//            Image scaledImage = originalImage.getScaledInstance(newWidth, newHeight, higherQuality ? Image.SCALE_SMOOTH : Image.SCALE_FAST);
//            if (scaledImage instanceof BufferedImage) {
//                return (BufferedImage) scaledImage;
//            } else {
//                BufferedImage newImage = new BufferedImage(newWidth, newHeight, type);
//
//                Graphics2D g2 = newImage.createGraphics();
//                g2.drawImage(scaledImage, 0, 0, null);
//                g2.dispose();
//
//                return newImage;
//            }
//        }

        BufferedImage newImage = originalImage;

        int tempWidth = newWidth;
        int tempHeight = newHeight;
        if (higherQuality) {
            // Use multi-step technique: start with original size, then scale down in multiple passes with drawImage()
            // until the target size is reached
            tempWidth = originalImage.getWidth();
            tempHeight = originalImage.getHeight();
        }

        do {
            if (higherQuality) {
                tempWidth = (tempWidth / 2 > newWidth) ? tempWidth / 2 : newWidth;
                tempHeight = (tempHeight / 2 > newHeight) ? tempHeight / 2 : newHeight;
            }

            BufferedImage tempImage = new BufferedImage(tempWidth, tempHeight, type);
            Graphics2D g2 = tempImage.createGraphics();
            g2.setRenderingHint(
                    KEY_INTERPOLATION,
                    higherQuality
//                            ? VALUE_INTERPOLATION_BICUBIC
                            ? VALUE_INTERPOLATION_BILINEAR
                            : VALUE_INTERPOLATION_NEAREST_NEIGHBOR
            );
            g2.drawImage(newImage, 0, 0, tempWidth, tempHeight, null);
            g2.dispose();

            newImage = tempImage;
        }
        while (tempWidth != newWidth || tempHeight != newHeight);

        return newImage;
    }

    public static BufferedImage resizeImage(BufferedImage originalImage, int newWidth, int newHeight) {
        return resizeImage(originalImage, newWidth, newHeight, true);
    }

    public static void storeImageAsPng(BufferedImage image, File file) {
        try {
            ImageIO.write(image, "png", file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
